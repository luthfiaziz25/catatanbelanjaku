import 'dart:async';

import 'package:catatan_belanjaku/ui_belanja/Beranda.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';

class Splashscreen extends StatefulWidget {
  @override
  _SplashscreenState createState() => _SplashscreenState();
}

class _SplashscreenState extends State<Splashscreen> {
  startTime() async {
    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, navigationPage);
  }


  final LocalStorage storage = new LocalStorage('DB_CATATAN_BELANJA');
  final LocalStorage storagecart = new LocalStorage('DB_CATATAN_CART');
  final LocalStorage storagebudget = new LocalStorage('DB_BUDGET');
  final LocalStorage storageselectedmore = new LocalStorage('DB_SELECTED');


  List temp =[];
  List barangdetail = [];
  List tempcart = [];
  List cart = [];
  List selectmore = [];
  var budgetku;

  void navigationPage() {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Beranda(cartfromlocal: cart,datafromlocal: barangdetail,budget: budgetku,selectedmore: selectmore)));
  }
  @override
  void initState() {
    startTime();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    List data = storage.getItem('listbarang');
    List listdata = storagecart.getItem('listcart');
    List selected = storageselectedmore.getItem('select');

    var  budget = storagebudget.getItem('budget');
    if(budget.toString().length != 0){
      setState(() {
        budgetku = budget;
      });
    }
    if(data != null){
      temp = data;
      if(temp.length != null){
        setState(() {
          barangdetail = temp;
        });

      }else{
        setState(() {
          barangdetail = [];
        });
      }
    }else{
      setState(() {
        barangdetail = [];
      });
    }
    if(listdata != null){
      tempcart = listdata;
      if(tempcart.length != null){
        setState(() {
          cart = tempcart;
        });
      }else{
        setState(() {
          cart = [];
        });
      }
    }else{
      setState(() {
        cart = [];
      });
    }
    if (selected != null) {
      setState(() {
        selectmore = selected;
      });
    }else{
      setState(() {
        selectmore = [];
      });
    }
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        minimum: EdgeInsets.only(top: 10),
        bottom: false,
        top: false,
        child: Container(
//          color: Colors.grey,
          alignment: Alignment.center,
          padding: EdgeInsets.all(20),
          child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Container(
//                color: Colors.white,
                height: MediaQuery.of(context).size.height - 100,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        alignment: Alignment.center,
                        height: 200,
                        width: 200,
                        padding: EdgeInsets.all(20) ,
                        decoration: BoxDecoration(
                          color: Colors.orangeAccent,
                          borderRadius: BorderRadius.circular(100),
                        ),
                        child: Image.asset('lib/assets/cartku.png',fit: BoxFit.cover)),
                    SizedBox(height: 20),
                    Text("Catatan Belanjaku",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.black),),
                    Text("- Save Money but Still Enjoy Your Life -",style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold,color: Colors.black),),
                  ],
                ),
              ),
              Container(
                height: 20,
                child: Text("versi 1.0.0",style: TextStyle(fontSize: 11,fontWeight: FontWeight.bold,color: Colors.black)),
              ),
            ],
          ),
        ),
      )
    );
  }
}
