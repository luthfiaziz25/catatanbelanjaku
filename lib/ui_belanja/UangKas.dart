import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class UangKas extends StatefulWidget {
  @override
  _UangKasState createState() => _UangKasState();
}

class _UangKasState extends State<UangKas> {
  WebViewController _webViewController;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Container(
          width: 50,
          height: 50,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(25),
//                        color: Colors.amber,
          ),
          child: FlatButton(
              padding: const EdgeInsets.all(0),
              child: Container(
                alignment: Alignment.center,
                child: Icon(Icons.arrow_back_ios,size: 20),
              ),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
              splashColor: Colors.amberAccent,
              onPressed: (){
                Navigator.pop(context);
              }),
        ),
        title: Text(
          "Uang Kas",
          style: TextStyle(color: Colors.black),
        ),
        iconTheme: IconThemeData(color: Colors.black),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: WebView(
        initialUrl:
            'https://docs.google.com/spreadsheets/d/1KUSUN_NZgtmBQzNgcXXvP6rqBbpCKQTHMiotxeYgRlA/edit?usp=sharing',
        javascriptMode: JavascriptMode.unrestricted,
        onWebViewCreated: (WebViewController webViewController) {
          _webViewController = webViewController;
        },
      ),
    );
  }
}
