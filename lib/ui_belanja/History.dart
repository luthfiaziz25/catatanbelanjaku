import 'package:catatan_belanjaku/ui_belanja/DetailHistory.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';

class History extends StatefulWidget {
  final datatranc;
  History({Key key,this.datatranc}) : super(key : key);
  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  final LocalStorage storagehistory = new LocalStorage('DB_HISTORY');

  List datatranc = [];
  clearStorage() {
    storagehistory.clear();
    setState(() {
      setState(() {
        datatranc = [];
      });
    });
  }

  deleteOneItem(_id){
    datatranc.removeWhere((item) => item['transaksi'] == _id);
    storagehistory.setItem('transaksi', datatranc);
    print(datatranc);
  }
  void _showOption() {
    showDialog(
        context: context,
        builder: (BuildContext context){
          return AlertDialog(
            title: Text("Anda Yakin Menghapus Semua History Transaksi ?"),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            content: Container(
              height: 35,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 9,
                    child : Container(
                        decoration: BoxDecoration(
                            color: Colors.red,
                            border: Border.all(
                                color: Colors.black,
                                width: 0.1
                            ),
                            borderRadius: BorderRadius.circular(10)
                        ),
                        height: 35,
                        child: FlatButton(
                          onPressed: (){
                            Navigator.pop(context);
                          },
                          child: Text("Cancel",style: TextStyle(color: Colors.white,fontSize: 12),),
                        )
                    ),
                  ),
                  Expanded(flex: 1,
                      child: Container()),
                  Expanded(
                    flex: 9,
                    child : Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: Colors.black,
                                width: 0.1
                            ),
                            borderRadius: BorderRadius.circular(10)
                        ),
                        height: 35,
                        child: FlatButton(
                          onPressed: (){
                            Navigator.pop(context);
                            clearStorage();
                          },
                          child: Text("Lanjutkan",style: TextStyle(fontSize: 12),),
                        )
                    ),
                  )
                ],
              ),
            ),
          );
        }
    );
  }
  void _showOptionDeleteOne(transaksi) {
    showDialog(
        context: context,
        builder: (BuildContext context){
          return AlertDialog(
            title: Text("Delete Item"),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            content: Container(
              height: 115,
              child: Column(
                children: <Widget>[
                  Container(
                    height: 70,
                    child: Text("Item yang dipilih merupakan transaksi pada ${transaksi.toString().replaceAll("T", " ")}",maxLines: 3,overflow: TextOverflow.ellipsis),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 9,
                        child : Container(
                            decoration: BoxDecoration(
                                color: Colors.red,
                                border: Border.all(
                                    color: Colors.black,
                                    width: 0.1
                                ),
                                borderRadius: BorderRadius.circular(10)
                            ),
                            height: 35,
                            child: FlatButton(
                              onPressed: (){
                                Navigator.pop(context);
                              },
                              child: Text("Cancel",style: TextStyle(color: Colors.white,fontSize: 12)),
                            )
                        ),
                      ),
                      Expanded(flex: 1,
                          child: Container()),
                      Expanded(
                        flex: 9,
                        child : Container(
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.black,
                                    width: 0.1
                                ),
                                borderRadius: BorderRadius.circular(10)
                            ),
                            height: 35,
                            child: FlatButton(
                              onPressed: (){
                                Navigator.pop(context);
                                deleteOneItem(transaksi);
                                setState(() {
                                  datatranc = datatranc;
                                });
                              },
                              child: Text("Lanjutkan",style: TextStyle(fontSize: 12)),
                            )
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          );
        }
    );
  }
  @override
  void initState() {
    if(widget.datatranc.toString() != "[]"){
      setState(() {
        datatranc = widget.datatranc;
      });
    }
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    List _datatranc = storagehistory.getItem('transaksi');
    if(widget.datatranc == null || widget.datatranc.toString() == "[]"){
      if(_datatranc != null){
        setState(() {
          datatranc = _datatranc;
        });
      }
    }
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          top: false,
          bottom: false,
          minimum: EdgeInsets.only(top: 25),
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Column(
              children: <Widget>[
                Container(
                  height: 50.0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
//                        color: Colors.amber,
                        ),
                        child: FlatButton(
                            padding: const EdgeInsets.all(0),
                            child: Container(
                              alignment: Alignment.center,
                              child: Icon(Icons.arrow_back_ios,size: 20),
                            ),
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                            splashColor: Colors.amberAccent,
                            onPressed: (){
                              Navigator.pop(context);
                            }),
                      ),
                      Text("History Transaksi",style: TextStyle(fontSize: 20),),
                      Container(
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                        ),
                        child: datatranc.length == 0 ? Container() : FlatButton(
                              padding: const EdgeInsets.all(0),
                              child: Container(
                                alignment: Alignment.center,
                                child: Icon(Icons.delete_forever),
                              ),
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                              splashColor: Colors.amberAccent,
                              onPressed: (){
                                _showOption();
                              }),
                      ),
                    ],
                  ),
                ),
                datatranc.toString() == "[]" ? Container(
                    height: MediaQuery.of(context).size.height/1.2,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Icon(Icons.history,size: 100,color: Colors.grey[800],),
                          Text("History Transaksi Kosong",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.grey[800]),textAlign: TextAlign.center,),
                          Text("Silakan Berbelanja dan Simpan Transaksi Anda!",
                              style: TextStyle(fontSize: 13,fontWeight: FontWeight.bold,color: Colors.grey[800]),textAlign: TextAlign.center
                          )
                        ],
                      ),
                    )) : Container(
                  height: MediaQuery.of(context).size.height - 95,
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: datatranc.length,
                      itemBuilder: (BuildContext context,int index){
                        return Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                          child: GestureDetector(
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => DetailHistory(listBelanjaan: datatranc[index])));
                            },
                            onLongPress: (){
                              _showOptionDeleteOne(datatranc[index]['transaksi']);
                            },
                            child: Container(
                              alignment: Alignment.center,
                              height: 60,
                              decoration: BoxDecoration(
                                  color: Colors.grey[300],
                                  borderRadius: BorderRadius.circular(20)
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Expanded(
                                    flex: 2,
                                    child: Align(
                                        alignment: Alignment.centerRight,
                                        child: Icon(Icons.receipt,size: 35,)),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: VerticalDivider(
                                      color: Colors.black,
                                    ),
                                  ),
                                  Expanded(
                                    flex: 11,
                                    child: Text("${datatranc[index]['transaksi'].toString().substring(0,10)}",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold)),
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
