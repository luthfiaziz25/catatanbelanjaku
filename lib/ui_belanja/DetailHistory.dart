import 'package:flutter/material.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:permission_handler/permission_handler.dart';
import 'package:share_extend/share_extend.dart';

class DetailHistory extends StatefulWidget {
  final listBelanjaan;
  DetailHistory({Key key,this.listBelanjaan}) : super(key : key);
  @override
  _DetailHistoryState createState() => _DetailHistoryState();
}

class _DetailHistoryState extends State<DetailHistory> {
  var dir;
  String fileName ="";
  String pathStorageAndroid;
  String pathStorageIos;
  List data = [];
  List listbelanjaan = [];
  FlutterMoneyFormatter fmf = FlutterMoneyFormatter(amount: 12345678.9012345);

  void Export(tanggal,data,lengthlist,budget,subtotal,potongan,sisa) async {
    var dirToSave;
    var directory;
    final pdf = pw.Document();
    pdf.addPage(pw.MultiPage(
        pageFormat: PdfPageFormat.a4,
        build: (pw.Context context) => <pw.Widget>[
          pw.Header(
              level: 1, text: 'Rincian Belanja Pada Tanggal ${tanggal.toString().replaceAll("T", " ")}'),
          pw.Container(
            child : pw.Row(
              children: <pw.Widget>[
                pw.Expanded(
                  flex: 2,
                  child: pw.Text("No")
                ),
                pw.Expanded(
                    flex: 5,
                    child: pw.Text("Barang")
                ),
                pw.Expanded(
                    flex: 4,
                    child: pw.Text("Harga")
                ),
                pw.Expanded(
                    flex: 3,
                    child: pw.Text("Banyak")
                ),
                pw.Expanded(
                    flex: 4,
                    child: pw.Text("Potongan")
                ),
                pw.Expanded(
                    flex: 4,
                    child: pw.Text("Sub Total")
                ),
              ]
            )
          ),
          pw.Container(
            child: pw.ListView.builder(
                itemCount: lengthlist,
                itemBuilder:(pw.Context context,int index){
                  return pw.Container(
                      child : pw.Row(
                          children: <pw.Widget>[
                            pw.Expanded(
                                flex: 2,
                                child: pw.Text("${index+1}")
                            ),
                            pw.Expanded(
                                flex: 5,
                                child: pw.Text("${data[index]['name']}")
                            ),
                            pw.Expanded(
                                flex: 4,
                                child: pw.Text("${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:double.parse(listbelanjaan[index]['harga'].toString())).output.symbolOnLeft}")
                            ),
                            pw.Expanded(
                                flex: 3,
                                child: pw.Text("${data[index]['qty']} ${data[index]['satuan']}")
                            ),
                            pw.Expanded(
                                flex: 4,
                                child: pw.Text("${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:double.parse(listbelanjaan[index]['potongan'].toString())).output.symbolOnLeft}")
                            ),
                            pw.Expanded(
                                flex: 4,
                                child: pw.Text("${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:double.parse(listbelanjaan[index]['hargatotal'].toString())).output.symbolOnLeft}")
                            ),
                          ]
                      )
                  );
            })
          ),
          pw.Padding(padding: const pw.EdgeInsets.all(10)),
          pw.Container(
            child: pw.Column(
              children: <pw.Widget>[
                pw.Row(
                  children: <pw.Widget>[
                    pw.Expanded(flex: 3,
                        child: pw.Text("Budget",style: pw.TextStyle(fontWeight: pw.FontWeight.bold,fontSize: 15))),
                    pw.Expanded(flex: 1,
                        child: pw.Text(":",style: pw.TextStyle(fontWeight: pw.FontWeight.bold,fontSize: 15))),
                    pw.Expanded(flex: 6,
                        child: pw.Text("${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:double.parse(budget)).output.symbolOnLeft}",style: pw.TextStyle(fontWeight: pw.FontWeight.bold,fontSize: 15))),
                  ],
                ),
                pw.Row(
                  children: <pw.Widget>[
                    pw.Expanded(flex: 3,
                        child: pw.Text("Total",style: pw.TextStyle(fontWeight: pw.FontWeight.bold,fontSize: 15))),
                    pw.Expanded(flex: 1,
                        child: pw.Text(":",style: pw.TextStyle(fontWeight: pw.FontWeight.bold,fontSize: 15))),
                    pw.Expanded(flex: 6,
                        child: pw.Text("${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:double.parse(subtotal)).output.symbolOnLeft}",style: pw.TextStyle(fontWeight: pw.FontWeight.bold,fontSize: 15))),
                  ],
                ),
                pw.Row(
                  children: <pw.Widget>[
                    pw.Expanded(flex: 3,
                        child: pw.Text("Potongan",style: pw.TextStyle(fontWeight: pw.FontWeight.bold,fontSize: 15))),
                    pw.Expanded(flex: 1,
                        child: pw.Text(":",style: pw.TextStyle(fontWeight: pw.FontWeight.bold,fontSize: 15))),
                    pw.Expanded(flex: 6,
                        child: pw.Text("${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:double.parse(potongan)).output.symbolOnLeft}",style: pw.TextStyle(fontWeight: pw.FontWeight.bold,fontSize: 15))),
                  ],
                ),
                pw.Row(
                  children: <pw.Widget>[
                    pw.Expanded(flex: 3,
                        child: pw.Text("Sisa",style: pw.TextStyle(fontWeight: pw.FontWeight.bold,fontSize: 15))),
                    pw.Expanded(flex: 1,
                        child: pw.Text(":",style: pw.TextStyle(fontWeight: pw.FontWeight.bold,fontSize: 15))),
                    pw.Expanded(flex: 6,
                        child: pw.Text("${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:double.parse(sisa)).output.symbolOnLeft}",style: pw.TextStyle(fontWeight: pw.FontWeight.bold,fontSize: 15))),
                  ],
                ),
              ],
            ),
          ),

        ]));
    if (Platform.isIOS) {
      dirToSave = await getApplicationDocumentsDirectory();
      directory = dirToSave.path;
      setState(() {
        pathStorageIos = "${directory}/";
      });
    } else{
      final PermissionHandler _permissionHandler = PermissionHandler();
      var result = await _permissionHandler.requestPermissions([PermissionGroup.storage]);
      if (result[PermissionGroup.storage] != PermissionStatus.granted) {
        Fluttertoast.showToast(
            msg: "Download Failed, Access Not granted",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb : 3,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
//        pr.hide();
        return;
      }
      //directory = await getExternalStorageDirectory();
      directory = '/storage/emulated/0/Download';
      setState(() {
        pathStorageAndroid = "${directory}/";
      });
    }
    final File file = File('${directory}/ArsipBelanja${tanggal}.pdf');
    fileName = "ArsipBelanja${tanggal}.pdf";
    await file.writeAsBytes(pdf.save()).then((e){
      _showOptionToOpenFile(context,file);
    }).catchError((f){
      Fluttertoast.showToast(
          msg: "Download Failed, Access Not granted",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb : 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    });
  }

  void _showOptionToOpenFile(context,path){
    String tmpfile = path.toString();
    int lengthtmp = tmpfile.length;
    String file = path.toString().substring(7,lengthtmp-1);
    showDialog(
        context: context,
        builder: (BuildContext context){
          return AlertDialog(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("Berhasil Disimpan"),
                Container(
                  width: 25,
                  height: 25,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
//                    color: Colors.amber,
                  ),
                  child: FlatButton(
                      padding: const EdgeInsets.all(0),
                      child: Container(
                        alignment: Alignment.center,
                        child: Icon(Icons.close,size: 25),
                      ),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                      splashColor: Colors.amberAccent,
                      onPressed: (){
                        Navigator.pop(context);
                      }),
                ),
              ],
            ),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            content: Container(
              height: 125,
              child: Column(
                children: <Widget>[
                  Container(
                    height: 80,
                    child: Text("Tersimpan Di ${file}",textAlign: TextAlign.left,maxLines: 3,overflow: TextOverflow.ellipsis,),
                  ),
                  SizedBox(height: 10,),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 9,
                        child : Container(
                            decoration: BoxDecoration(
                                color: Colors.red,
                                border: Border.all(
                                    color: Colors.black,
                                    width: 0.1
                                ),
                                borderRadius: BorderRadius.circular(10)
                            ),
                            height: 35,
                            child: FlatButton(
                              onPressed: (){
                                Navigator.pop(context);
                                if(Platform.isIOS){
                                  ShareExtend.share(pathStorageIos + fileName,"file");
                                }else{
                                  ShareExtend.share(pathStorageAndroid + fileName,"file");
                                }
                              },
                              child: Text("Share",style: TextStyle(color: Colors.white),),
                            )
                        ),
                      ),
                      Expanded(flex: 1,
                          child: Container()),
                      Expanded(
                        flex: 9,
                        child : Container(
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.black,
                                    width: 0.1
                                ),
                                borderRadius: BorderRadius.circular(10)
                            ),
                            height: 35,
                            child: FlatButton(
                              onPressed: (){
                                Navigator.pop(context);
                                if(Platform.isIOS){
                                  OpenFile.open(pathStorageIos + fileName);
                                }else{
                                  OpenFile.open(pathStorageAndroid + fileName);
                                }
                              },
                              child: Text("Open"),
                            )
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          );
        }
    );
  }
  @override
  void initState() {
    setState(() {
      data.add(widget.listBelanjaan);
      listbelanjaan = data[0]['data'];
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: false,
        bottom: false,
        minimum: EdgeInsets.only(top: 25),
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              Container(
                height: 50.0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
//                        color: Colors.amber,
                      ),
                      child: FlatButton(
                          padding: const EdgeInsets.all(0),
                          child: Container(
                            alignment: Alignment.center,
                            child: Icon(Icons.arrow_back_ios,size: 20),
                          ),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                          splashColor: Colors.amberAccent,
                          onPressed: (){
                            Navigator.pop(context);
                          }),
                    ),
                    Text("Detail History",style: TextStyle(fontSize: 20),),
                    Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
//                        color: Colors.amber,
                      ),
                      child: FlatButton(
                          padding: const EdgeInsets.all(0),
                          child: Container(
                            alignment: Alignment.center,
                            child: Icon(Icons.file_download),
                          ),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                          splashColor: Colors.amberAccent,
                          onPressed: (){
                            Export(data[0]['transaksi'],listbelanjaan,listbelanjaan.length,data[0]['budget'],data[0]['jumlahakhir'],data[0]['jumlahpotongan'],data[0]['sisaakhir']);
                          }),
                    ),
                  ],
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height - 200,
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: listbelanjaan.length,
                    itemBuilder: (BuildContext context,int index){
                      return Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.grey[300]
                          ),
                          padding: EdgeInsets.all(10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                child: Row(children: <Widget>[
                                  Expanded(flex: 3,
                                      child: Text("Barang")),
                                  Expanded(flex: 1,
                                      child: Text(":")),
                                  Expanded(flex: 8,
                                      child: Text("${listbelanjaan[index]['name']}",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),)),
                                ]),
                              ),
                              Divider(color: Colors.black),
                              Container(
                                child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Expanded(flex: 3,
                                          child: Text("Rincian")),
                                      Expanded(flex: 1,
                                          child: Text(":")),
                                      Expanded(flex: 8,
                                          child: Container(
                                            child: Column(
                                              children: <Widget>[
                                                Container(
                                                  child: Row(
                                                    children: <Widget>[
                                                      Expanded(
                                                        flex: 3,
                                                        child: Text("${listbelanjaan[index]['qty']} ${listbelanjaan[index]['satuan']}"),
                                                      ),
                                                      Expanded(flex: 2,
                                                          child: Text("X")),
                                                      Expanded(
                                                        flex: 10,
                                                        child: Text("Harga Satuan ${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:listbelanjaan[index]['hargasatuan']).output.symbolOnLeft}"),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Divider(color: Colors.black,),
                                                Container(
                                                  child: Row(
                                                    children: <Widget>[
                                                      Expanded(
                                                        flex: 1,
                                                        child: Text("=",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
                                                      ),
                                                      Expanded(flex: 8,
                                                          child: Text("${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:double.parse(listbelanjaan[index]['hargatotal'].toString())).output.symbolOnLeft}",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold))),
                                                    ],
                                                  ),
                                                ),
                                                Divider(color: Colors.black,),
                                                Container(
                                                  child: Row(
                                                    children: <Widget>[
                                                      Expanded(
                                                        flex: 6,
                                                        child: Text("Potongan"),
                                                      ),
                                                      Expanded(flex: 1,
                                                          child: Text(":")),
                                                      Expanded(flex: 11,
                                                          child: Text("${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:listbelanjaan[index]['potongan']).output.symbolOnLeft}",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold))),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  child: Row(
                                                    children: <Widget>[
                                                      Expanded(
                                                        flex: 6,
                                                        child: Text("SubTotal"),
                                                      ),
                                                      Expanded(flex: 1,
                                                          child: Text(":")),
                                                      Expanded(flex: 11,
                                                          child: Text("${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:listbelanjaan[index]['sisapotongan']).output.symbolOnLeft}",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold))),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )),
                                    ]),
                              ),
                            ],
                          ),
                        ),
                      );
                    }),
              ),
              Container(
                height: 105,
                alignment: Alignment.centerRight,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(flex: 3,
                            child: Text("Budget",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16))),
                        Expanded(flex: 1,
                            child: Text(":",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16))),
                        Expanded(flex: 6,
                            child: Text("${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:double.parse(data[0]['budget'])).output.symbolOnLeft}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16))),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(flex: 3,
                            child: Text("Total",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16))),
                        Expanded(flex: 1,
                            child: Text(":",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16))),
                        Expanded(flex: 6,
                            child: Text("${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:double.parse(data[0]['jumlahakhir'])).output.symbolOnLeft}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16))),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(flex: 3,
                            child: Text("Potongan",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16))),
                        Expanded(flex: 1,
                            child: Text(":",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16))),
                        Expanded(flex: 6,
                            child: Text("${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:double.parse(data[0]['jumlahpotongan'])).output.symbolOnLeft}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16))),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(flex: 3,
                            child: Text("Sisa",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16)
                            )),
                        Expanded(flex: 1,
                            child: Text(":",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16))),
                        Expanded(flex: 6,
                            child: Text("${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:double.parse(data[0]['sisaakhir'])).output.symbolOnLeft}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16,color: double.parse(data[0]['sisaakhir']) <= 0.0 ? Colors.red : Colors.black))),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
