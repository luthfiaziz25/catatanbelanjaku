import 'package:catatan_belanjaku/ui_belanja/History.dart';
import 'package:flutter/material.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:localstorage/localstorage.dart';

class Cart extends StatefulWidget {
  final List;
  final Inventory;
  final budget;
  final history;
  final selected;
  Cart({Key key,this.List,this.Inventory,this.budget,this.history,this.selected}) : super(key : key);
  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  List data;
  List listpotongan = [];
  List listharga = [];
  List transaksi = [];
  List satuan = ['kg','ons','gram','pcs','ikat','butir','buah'];
  List selected = [];

  double jumlahakhir = 0.0;
  double jumlahpotongan = 0.0;
  double budget = 0.0;
  double hargasatuan = 0.0;
  double subtotal = 0.0;
  double sisa = 0.0;
  double sisaakhir = 0.0;
  bool _loading = true;
  bool _cartempty = false;

  //ControllerTextEdit
  TextEditingController c_satuan = new TextEditingController();
  TextEditingController c_namabarang = TextEditingController();
  TextEditingController c_qty = TextEditingController();
  TextEditingController c_harga = TextEditingController();
  TextEditingController c_potongan = TextEditingController();

  //Dropdown
  List<DropdownMenuItem<String>> _dropDownSatuan;
  var valueSatuan;
  void changeSatuan(String selected) {
    if (selected != null || selected != '') {
      setState(() {
        valueSatuan = selected;
      });
    }
  }
  List<DropdownMenuItem<String>> buildAndGetDropDownMenuSatuan() {
    List<DropdownMenuItem<String>> items = new List();
    for (String data in satuan) {
      items.add(new DropdownMenuItem(value: data, child: new Text(data)));
    }
    return items;
  }

  //savetoStorage
  final LocalStorage storagecart = new LocalStorage('DB_CATATAN_CART');
  final LocalStorage storagebudget = new LocalStorage('DB_BUDGET');
  final LocalStorage storagehistory = new LocalStorage('DB_HISTORY');
  final LocalStorage storageselectedmore = new LocalStorage('DB_SELECTED');

  clearStorage() {
    storagecart.clear();
    setState(() {
      data.clear();
      listpotongan.clear();
      listharga.clear();
      jumlahpotongan = 0;
      jumlahakhir = 0;
      sisaakhir = 0;
      budget = 0;
      _loading = true;
    });
  }
  SaveCartToLocal() {
    storagecart.setItem('listcart', data);
  }
  SaveBudgetToLocal() {
    storagebudget.setItem('budget', budget);
  }
  SaveTransaction(time){
    var historytransaksi = {
      "transaksi" : "$time",
      "budget" : "$budget",
      "jumlahakhir" : "$jumlahakhir",
      "sisaakhir" : "$sisaakhir",
      "jumlahpotongan" : "$jumlahpotongan",
      "data" : data
    };
    transaksi.add(historytransaksi);
    storagehistory.setItem('transaksi', transaksi);
    storagecart.clear();
    storagebudget.setItem('budget', budget);
    setState(() {
      data.clear();
      listpotongan.clear();
      listharga.clear();
      jumlahpotongan = 0;
      jumlahakhir = 0;
      sisaakhir = 0;
      _loading = true;
    });
  }
  SaveSelectedToLocal() {
    storageselectedmore.setItem('select', selected);
  }
  editDataInCart(id,nama,qty,harga,satuan,potong){
    if(potong == null || potong == "0"){
      setState(() {
        potong = "0.0";
      });
    }
    double hargatotal = double.parse(harga) * double.parse(qty);
    double hargasatuan = double.parse(harga);
    double sisapotongan = hargatotal - double.parse(potong);
    final updatedata = data.firstWhere((item) => item['id'].toString() == id.toString());
    setState((){
      updatedata['name'] = nama;
      updatedata['qty'] = qty;
      updatedata['harga'] = double.parse(harga);
      updatedata['potongan'] = double.parse(potong);
      updatedata['hargatotal'] = hargatotal;
      updatedata['hargasatuan'] = hargasatuan;
      updatedata['sisapotongan'] = sisapotongan;
      updatedata['satuan'] = satuan.toString();
    });
    SaveCartToLocal();
    setState(() {
      listpotongan.clear();
      listharga.clear();
      jumlahakhir = 0.0;
      sisaakhir = 0.0;
      jumlahpotongan = 0.0;
    });
    getData();
  }
  DateTime timetransaction = DateTime.now();
  FlutterMoneyFormatter fmf = FlutterMoneyFormatter(amount: 12345678.9012345);
  TextEditingController c_money = TextEditingController();
  getData(){
    setState(() {
      if(widget.budget != null){
        budget = widget.budget;
      }
      data = widget.List;
      for(int i=0;i<data.length;i++){
        listpotongan.add(data[i]['potongan']);
        listharga.add(data[i]['sisapotongan']);
      }
      if(listharga.isNotEmpty){
        jumlahakhir = listharga.reduce((a,b)=>a+b);
        _loading = false;
      }
      if(listpotongan.isNotEmpty){
        jumlahpotongan = listpotongan.reduce((a,b)=>a+b);
      }
      if(widget.history != null){
        transaksi = widget.history;
      }
      sisaakhir = budget - jumlahakhir;
    });
  }
  _addMoney(){
    showDialog(context: context,
    builder: (BuildContext context){
      return AlertDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        title: Text("Add Budget"),
        content: Container(
          height: 80,
          child: Column(
            children: <Widget>[
              Container(
                height: 35,
                child: TextFormField(
                  controller: c_money,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    hintText: "Masukan Budget Anda",
                    fillColor: Colors.white,
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(10),
                      borderSide: new BorderSide(),
                    ),
                    contentPadding: EdgeInsets.only(left: 10,right: 10),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 35,
                alignment: Alignment.centerRight,
                child: FlatButton(
                    onPressed: (){
                      if(c_money.text.length == 0 || c_money.text == '0'){
                        Fluttertoast.showToast(
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            msg: "Tidak Boleh Kosong",
                            backgroundColor: Colors.red,
                            textColor: Colors.white
                        );
                      }else{
                        Navigator.pop(context);
                        setState(() {
                          budget = double.parse(c_money.text);
                          sisaakhir = budget - jumlahakhir;
                          SaveBudgetToLocal();
                        });
                      }
                    },
                    color: Colors.blue,
                    splashColor: Colors.amberAccent,
                    padding: EdgeInsets.zero,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    child: Container(
                      child: Text("Submit",style: TextStyle(fontSize: 14,color: Colors.white),),
                    )
                ),
              )
            ],
          )
        ),
      );
    });
  }
  deleteData(_id) {
    setState(() {
      data.removeWhere((item) => item['id'] == _id);
      selected.removeWhere((item) => item == _id);
      listpotongan.clear();
      listharga.clear();

      for(int i=0;i<data.length;i++){
        listpotongan.add(data[i]['potongan']);
        listharga.add(data[i]['sisapotongan']);
      }
      if(listharga.isNotEmpty){
        jumlahakhir = listharga.reduce((a,b)=>a+b);
        _loading = false;
      }else{
        jumlahakhir = 0;
        _loading = true;
        _cartempty = true;
      }
      if(listpotongan.isNotEmpty){
        jumlahpotongan = listpotongan.reduce((a,b)=>a+b);
      }else{
        jumlahpotongan = 0;
      }
      sisaakhir = budget - jumlahakhir;
      SaveCartToLocal();
      SaveSelectedToLocal();
    });
  }
  void _showAlertToDelete(context){
    showDialog(
        context: context,
        builder: (BuildContext context){
          return AlertDialog(
            title: Text("Apakah Anda Yakin Menghapus List Belanjaan Tanpa Menyimpan Ke History?",style: TextStyle(fontSize: 16),),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            content: Container(
              height: 35,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 9,
                    child : Container(
                        decoration: BoxDecoration(
                            color: Colors.red,
                            border: Border.all(
                                color: Colors.black,
                                width: 0.1
                            ),
                            borderRadius: BorderRadius.circular(10)
                        ),
                        height: 35,
                        child: FlatButton(
                          onPressed: (){
                            Navigator.pop(context);
                          },
                          child: Text("Cancel",style: TextStyle(color: Colors.white),),
                        )
                    ),
                  ),
                  Expanded(flex: 1,
                      child: Container()),
                  Expanded(
                    flex: 9,
                    child : Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: Colors.black,
                                width: 0.1
                            ),
                            borderRadius: BorderRadius.circular(10)
                        ),
                        height: 35,
                        child: FlatButton(
                          onPressed: (){
                            Navigator.pop(context);
                            clearStorage();
                            _cartempty = true;
                          },
                          child: Text("Lanjutkan"),
                        )
                    ),
                  )
                ],
              ),
            ),
          );
        }
    );
  }
  void _showOption(context,id,name,qty,satuan,harga,potongan){
    showDialog(
        context: context,
        builder: (BuildContext context){
          return AlertDialog(
            title: Text("$name"),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            content: Container(
              height: 80,
              child: Column(
                children: <Widget>[
                  Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.black,
                              width: 0.1
                          ),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      height: 35,
                      child: FlatButton(
                        onPressed: (){
                          Navigator.pop(context);
                          _showDialogEdit(context,id,name,qty,satuan,harga,potongan);
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween ,
                          children: <Widget>[
                            Text("Edit"),
                            Icon(Icons.edit),
                          ],
                        ),
                      )
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.black,
                              width: 0.1
                          ),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      height: 35,
                      child: FlatButton(
                        onPressed: (){
                          Navigator.pop(context);
                          deleteData(id);
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween ,
                          children: <Widget>[
                            Text("Delete"),
                            Icon(Icons.delete),
                          ],
                        ),
                      )
                  ),
                 /* SizedBox(
                    height: 10,
                  ),
                  Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.black,
                              width: 0.1
                          ),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      height: 35,
                      child: FlatButton(
                        onPressed: (){
                          Navigator.pop(context);
                          _showDialog(context, 'edit', id, name, qty, harga);
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween ,
                          children: <Widget>[
                            Text("Edit"),
                            Icon(Icons.mode_edit),
                          ],
                        ),
                      )
                  ),*/
                ],
              ),
            ),
          );
        }
    );
  }
  void _showDialogEdit(context,id,name,qty,satuan,harga,potongan) {
    print(harga);
    setState(() {
      var endpotongan = potongan.toString().length;
      var endharga = harga.toString().length;
      c_potongan.text = potongan.toString().substring(0,endpotongan-2);
      c_namabarang.text = name;
      c_harga.text = harga.toString().substring(0,endharga-2);
      c_qty.text = qty;
    });
    _dropDownSatuan = buildAndGetDropDownMenuSatuan();
    c_satuan.text = _dropDownSatuan[0].value;
    if(satuan.toString().length != 0){
      setState(() {
        c_satuan.text = satuan;
      });
    }
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return alert dialog object
        return AlertDialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          title: Text('Edit Data Barang'),
          content: SingleChildScrollView(
            child: Container(
              height: 210,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: 35,
                    child: TextFormField(
                      controller: c_namabarang,
                      decoration: InputDecoration(
                        hintText: "Masukan Nama Barang",
                        fillColor: Colors.white,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10),
                          borderSide: new BorderSide(),
                        ),
                        contentPadding: EdgeInsets.only(left: 10,right: 10),
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 9,
                          child: Container(
                            height: 35,
                            child: TextFormField(
                              controller: c_qty,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                hintText: "Masukan Quantity",
                                fillColor: Colors.white,
                                border: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(10),
                                  borderSide: new BorderSide(),
                                ),
                                contentPadding: EdgeInsets.only(left: 10,right: 10),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: SizedBox(width: 5),
                        ),
                        Expanded(
                          flex: 5,
                          child : Container(
                            height: 35,
                            width: MediaQuery.of(context).size.longestSide,
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                isExpanded: true,
                                value: c_satuan.text,
                                items: _dropDownSatuan,
                                onChanged:(select){
                                  if (select.toString() != null || select.toString() != '') {
                                    setState(() {
                                      Navigator.of(context).pop();
                                      _showDialogEdit(context,"$id","$name","$qty","$satuan","$harga","$potongan");
                                      valueSatuan = select.toString();
                                      c_satuan.text = select.toString();
                                    });
                                  }
                                },
                                hint: Text("Satuan"),
                              ),
                            ),
                            decoration: new BoxDecoration(
                              border: new Border.all(color: Colors.black38),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            padding: const EdgeInsets.only(left: 10),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    height: 35,
                    child: TextFormField(
                      controller: c_harga,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        hintText: "Masukan Harga Barang",
                        fillColor: Colors.white,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10),
                          borderSide: new BorderSide(),
                        ),
                        contentPadding: EdgeInsets.only(left: 10,right: 10),
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  Container(
                    height: 35,
                    child: TextFormField(
                      controller: c_potongan,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        hintText: "Masukan Potongan",
                        fillColor: Colors.white,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10),
                          borderSide: new BorderSide(),
                        ),
                        contentPadding: EdgeInsets.only(left: 10,right: 10),
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    alignment: Alignment.centerRight,
                    height: 30,
                    child: FlatButton(
                        onPressed: (){
                          if(c_satuan.text.length == 0 || c_namabarang.text.length == 0 ||
                              c_qty.text.length == 0 || c_harga.text.length == 0){
                            Fluttertoast.showToast(
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                msg: "Semua Field Harus Diisi !",
                                backgroundColor: Colors.red,
                                textColor: Colors.white
                            );
                          }else{
                            Navigator.pop(context);
                            if(c_potongan.text.length == 0){
                              setState(() {
                                c_potongan.text = "0";
                              });
                            }
                            setState(() {
                              id = id;
                              name = c_namabarang.text;
                              qty = c_qty.text;
                              harga = c_harga.text;
                              potongan = c_potongan.text;
                            });
                            editDataInCart(id,name,qty,harga,satuan,potongan);
                            c_harga.clear();
                            c_qty.clear();
                            c_namabarang.clear();
                            c_satuan.clear();
                            c_potongan.clear();
                          }
                        },
                        color: Colors.blue,
                        splashColor: Colors.amberAccent,
                        padding: EdgeInsets.zero,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 5,vertical: 3),
                          child: Text("Add To Cart",style: TextStyle(fontSize: 14,color: Colors.white),),
                        )
                    )
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
  void _showOptionSaveOrDelete(){
    showDialog(
        context: context,
        builder: (BuildContext context){
          return AlertDialog(
            title: Text("Sudah Selesai Belanja ?"),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            content: Container(
              height: 80,
              child: Column(
                children: <Widget>[
                  Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.black,
                              width: 0.1
                          ),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      height: 35,
                      child: FlatButton(
                        onPressed: (){
                          Navigator.pop(context);
                          SaveTransaction(timetransaction.toIso8601String().substring(0,19));
                          _cartempty = true;
                          _showToOpenPageHistory(context);
                          storageselectedmore.clear();
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween ,
                          children: <Widget>[
                            Text("Save Transaction"),
                            Icon(Icons.save_alt),
                          ],
                        ),
                      )
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.black,
                              width: 0.1
                          ),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      height: 35,
                      child: FlatButton(
                        onPressed: (){
                          Navigator.pop(context);
                          _showAlertToDelete(context);
                          storageselectedmore.clear();
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween ,
                          children: <Widget>[
                            Text("Delete"),
                            Icon(Icons.delete),
                          ],
                        ),
                      )
                  ),
                  /* SizedBox(
                    height: 10,
                  ),
                  Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.black,
                              width: 0.1
                          ),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      height: 35,
                      child: FlatButton(
                        onPressed: (){
                          Navigator.pop(context);
                          _showDialog(context, 'edit', id, name, qty, harga);
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween ,
                          children: <Widget>[
                            Text("Edit"),
                            Icon(Icons.mode_edit),
                          ],
                        ),
                      )
                  ),*/
                ],
              ),
            ),
          );
        }
    );
  }
  void _showToOpenPageHistory(context){
    showDialog(
        context: context,
        builder: (BuildContext context){
          return AlertDialog(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("Berhasil Disimpan"),
                Container(
                  width: 25,
                  height: 25,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
//                    color: Colors.amber,
                  ),
                  child: FlatButton(
                      padding: const EdgeInsets.all(0),
                      child: Container(
                        alignment: Alignment.center,
                        child: Icon(Icons.close,size: 25),
                      ),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                      splashColor: Colors.amberAccent,
                      onPressed: (){
                        Navigator.pop(context);
                      }),
                ),
              ],
            ),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            content: Container(
              height: 125,
              child: Column(
                children: <Widget>[
                  Container(
                    height: 80,
                    child: Text("Transaksi Berhasil Disimpan Dihistory Transaksi!",textAlign: TextAlign.left,maxLines: 3,overflow: TextOverflow.ellipsis,),
                  ),
                  SizedBox(height: 10,),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 9,
                        child : Container(
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.black,
                                    width: 0.1
                                ),
                                borderRadius: BorderRadius.circular(10)
                            ),
                            height: 35,
                            child: FlatButton(
                              onPressed: (){
                                Navigator.pop(context);
                                Navigator.pop(context);
                              },
                              child: Text("Close",style: TextStyle(color: Colors.black),),
                            )
                        ),
                      ),
                      Expanded(flex: 1,
                          child: Container()),
                      Expanded(
                        flex: 9,
                        child : Container(
                            decoration: BoxDecoration(
                                color: Colors.red,
                                border: Border.all(
                                    color: Colors.black,
                                    width: 0.1
                                ),
                                borderRadius: BorderRadius.circular(10)
                            ),
                            height: 35,
                            child: FlatButton(
                              onPressed: (){
                                Navigator.pop(context);
                                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>History()));
                              },
                              child: Text("Open",style: TextStyle(color: Colors.white)),
                            )
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          );
        }
    );
  }
  @override
  void initState() {
//    print(widget.List);
    setState(() {
      selected = widget.selected;
    });
    getData();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          minimum: EdgeInsets.only(top: 25),
          top: false,
          bottom: false,
          child: Padding(
            padding: const EdgeInsets.only(left: 10,right: 10,top: 10),
            child: Column(
              children: <Widget>[
                Container(
                  height: 50.0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                        ),
                        child: FlatButton(
                            padding: const EdgeInsets.all(0),
                            child: Container(
                              alignment: Alignment.center,
                              child: Icon(Icons.arrow_back_ios,size: 20),
                            ),
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                            splashColor: Colors.amberAccent,
                            onPressed: (){
                              Navigator.pop(context);
                            }),
                      ),
                      Text("Cart",style: TextStyle(fontSize: 20),),
                      Container(
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                        ),
                        child: _cartempty ? Container() : FlatButton(
                            padding: const EdgeInsets.all(0),
                            child: Container(
                              alignment: Alignment.center,
                              child: Icon(Icons.more_vert),
                            ),
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                            splashColor: Colors.amberAccent,
                            onPressed: (){
                              _showOptionSaveOrDelete();
                            }),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height - 190,
                  child: _loading ?  Container(
                      height: MediaQuery.of(context).size.height/1.2,
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Icon(Icons.remove_shopping_cart,size: 100,color: Colors.grey[800],),
                            Text("Keranjang Kosong",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.grey[800]),textAlign: TextAlign.center),
                            Text("Tambahkan Barang Dengan Tap List Barang",
                                style: TextStyle(fontSize: 13,fontWeight: FontWeight.bold,color: Colors.grey[800]),textAlign: TextAlign.center
                            )
                          ],
                        ),
                      )) : ListView.builder(
                      itemCount: data.length,
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, int index){
                        return Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5),
                          child: GestureDetector(
                            onTap: (){
                              _showOption(context,data[index]['id'],data[index]['name'],data[index]['qty'],data[index]['satuan'],data[index]['harga'],data[index]['potongan']);
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: Colors.grey[300]
                              ),
                              padding: EdgeInsets.all(10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    child: Row(children: <Widget>[
                                      Expanded(flex: 3,
                                          child: Text("Barang")),
                                      Expanded(flex: 1,
                                          child: Text(":")),
                                      Expanded(flex: 8,
                                          child: Text("${data[index]['name']}",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),)),
                                    ]),
                                  ),
                                  Divider(color: Colors.black),
                                  Container(
                                    child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Expanded(flex: 3,
                                              child: Text("Rincian")),
                                          Expanded(flex: 1,
                                              child: Text(":")),
                                          Expanded(flex: 8,
                                              child: Container(
                                                child: Column(
                                                  children: <Widget>[
                                                    Container(
                                                      child: Row(
                                                        children: <Widget>[
                                                          Expanded(
                                                            flex: 2,
                                                            child: Text("${data[index]['qty']} ${data[index]['satuan']}"),
                                                          ),
                                                          Expanded(flex: 2,
                                                              child: Text("X")),
                                                          Expanded(
                                                            flex: 10,
                                                            child: Text("Harga Satuan ${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:data[index]['hargasatuan']).output.symbolOnLeft}"),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Divider(color: Colors.black,),
                                                    Container(
                                                      child: Row(
                                                        children: <Widget>[
                                                          Expanded(
                                                            flex: 1,
                                                            child: Text("=",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
                                                          ),
                                                          Expanded(flex: 8,
                                                              child: Text("${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:double.parse(data[index]['hargatotal'].toString())).output.symbolOnLeft}",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold))),
                                                        ],
                                                      ),
                                                    ),
                                                    Divider(color: Colors.black,),
                                                    Container(
                                                      child: Row(
                                                        children: <Widget>[
                                                          Expanded(
                                                            flex: 5,
                                                            child: Text("Potongan"),
                                                          ),
                                                          Expanded(flex: 1,
                                                              child: Text(":")),
                                                          Expanded(flex: 10,
                                                              child: Text("${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:data[index]['potongan']).output.symbolOnLeft}",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold))),
                                                        ],
                                                      ),
                                                    ),
                                                    Container(
                                                      child: Row(
                                                        children: <Widget>[
                                                          Expanded(
                                                            flex: 5,
                                                            child: Text("SubTotal"),
                                                          ),
                                                          Expanded(flex: 1,
                                                              child: Text(":")),
                                                          Expanded(flex: 10,
                                                              child: Text("${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:data[index]['sisapotongan']).output.symbolOnLeft}",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold))),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )),
                                        ]),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    height: 105,
                    alignment: Alignment.centerRight,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(flex: 3,
                                child: Text("Budget",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16))),
                            Expanded(flex: 1,
                                child: Text(":",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16))),
                            Expanded(flex: 6,
                                child: Text("${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:budget).output.symbolOnLeft}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16))),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(flex: 3,
                                child: Text("Total",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16))),
                            Expanded(flex: 1,
                                child: Text(":",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16))),
                            Expanded(flex: 6,
                                child: Text("${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:jumlahakhir).output.symbolOnLeft}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16))),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(flex: 3,
                                child: Text("Potongan",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16))),
                            Expanded(flex: 1,
                                child: Text(":",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16))),
                            Expanded(flex: 6,
                                child: Text("${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:jumlahpotongan).output.symbolOnLeft}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16))),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(flex: 3,
                                child: Text("Sisa",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16)
                                )),
                            Expanded(flex: 1,
                                child: Text(":",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16))),
                            Expanded(flex: 6,
                                child: Text("${fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:sisaakhir).output.symbolOnLeft}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16,color: sisaakhir <= 0.0 ? Colors.red : Colors.black))),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: Container(
        alignment: Alignment.bottomRight,
        child: Container(
          width: 50,
          height: 50,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(25),
            color: _cartempty ? Colors.grey : Colors.blue,
          ),
          child: FlatButton(
              padding: const EdgeInsets.all(0),
              child: Container(
                alignment: Alignment.center,
                child: Icon(Icons.attach_money,size: 25,color: Colors.white,),
              ),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
              splashColor: _cartempty ? Colors.white : Colors.amberAccent,
              onPressed: (){
                _cartempty ? Fluttertoast.showToast(
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    msg: "Cart Kosong, Silakan Tambahkan Cart Dengan Memilih List Barang",
                    backgroundColor: Colors.orange,
                    textColor: Colors.white
                ) : _addMoney();
              }),
        ),
      ),
    );
  }
}
