import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:catatan_belanjaku/ui_belanja/Cart.dart';
import 'package:catatan_belanjaku/ui_belanja/History.dart';
import 'package:catatan_belanjaku/ui_belanja/UangKas.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:localstorage/localstorage.dart';
import 'package:path_provider/path_provider.dart';

class Beranda extends StatefulWidget {
  List datafromlocal;
  List cartfromlocal;
  List selectedmore;
  final budget;
  Beranda({Key key, this.cartfromlocal, this.datafromlocal, this.budget,this.selectedmore})
      : super(key: key);
  @override
  _BerandaState createState() => _BerandaState();
}

class _BerandaState extends State<Beranda> {
  @override
  void initState() {
    setState(() {
      barangdetail = widget.datafromlocal;
      cart = widget.cartfromlocal;
      _selectedBarang = widget.selectedmore;
//      print(_selectedBarang);
      if (barangdetail.isNotEmpty) {
        _loading = false;
      }
    });
    _dropDownSatuan = buildAndGetDropDownMenuSatuan();
    c_satuan.text = _dropDownSatuan[0].value;
    super.initState();
  }

  var namabarang;
  var budgetku;
  int qty;
  double harga;
  List barangdetail = [];
  List satuan = ['kg', 'ons', 'gram', 'pcs', 'ikat', 'butir', 'buah'];
  List cart = [];

  List temp = [];
  List tempcart = [];
  List tempsearch = [];

  bool _loading = true;
  bool add = false;
  bool visible = false;
  bool moreselection = false;
  bool onlongpress = false;
  bool selectall = false;

  //partof history tranc
  List datatranc = [];
  List timetranc = [];

  TextEditingController c_satuan = new TextEditingController();
  TextEditingController c_namabarang = TextEditingController();
  TextEditingController c_qty = TextEditingController();
  TextEditingController c_harga = TextEditingController();
  TextEditingController c_potongan = TextEditingController();
  TextEditingController c_editingController = TextEditingController();
  var valueSatuan;
  void changeSatuan(String selected) {
    if (selected != null || selected != '') {
      setState(() {
        valueSatuan = selected;
      });
    }
  }

  List<DropdownMenuItem<String>> _dropDownSatuan;

  List<DropdownMenuItem<String>> buildAndGetDropDownMenuSatuan() {
    List<DropdownMenuItem<String>> items = new List();
    for (String data in satuan) {
      items.add(new DropdownMenuItem(value: data, child: new Text(data)));
    }
    return items;
  }

  List _selectedBarang = List();
  List tmpselect = [];

  _selectAll(check){
    print(check);
   if(check == true){
     setState(() {
       _selectedBarang.clear();
       cart.clear();
       List _id = [];
       for (int i = 0; i < cart.length; i++) {
         setState(() {
           _id.add(cart[i]['id']);
         });
       }
       for(int i=0;i<barangdetail.length;i++){
         _selectedBarang.add(barangdetail[i]['id']);
       }
       for(int i=0;i<_selectedBarang.length;i++){
         _selectedBarang.contains(barangdetail[i]['id']);
         bool isPresent(String idcart) {
           return _id.toString().contains(_selectedBarang[i].toString());
         }
         if (isPresent(_selectedBarang[i].toString()) == false) {
           cart.add({
             "id": barangdetail[i]['id'],
             "name": barangdetail[i]['name'],
             "qty": barangdetail[i]['qty'],
             "harga": double.parse(barangdetail[i]['harga']),
             "satuan": barangdetail[i]['satuan'],
             "potongan": 0.0,
             "hargatotal": double.parse(barangdetail[i]['harga']) * double.parse(barangdetail[i]['qty']),
             "hargasatuan": double.parse(barangdetail[i]['harga']),
             "sisapotongan": double.parse(barangdetail[i]['harga']) * double.parse(barangdetail[i]['qty'])
           });
         }
       }
       SaveCartToLocal();
       SaveSelectedToLocal();
     });
   }
   else{
     setState(() {
       _selectedBarang.clear();
       cart.clear();
     });
     SaveCartToLocal();
     SaveSelectedToLocal();
   }
  }
  void _onBarangSelected(bool selected, category_id) {
    List _id = [];
    for (int i = 0; i < cart.length; i++) {
      setState(() {
        _id.add(cart[i]['id']);
      });
    }
    if (selected == true) {
      setState(() {
        _selectedBarang.add(category_id);
        if(barangdetail.length == _selectedBarang.length){
           selectall = true;
        }
        for(int i=0;i<_selectedBarang.length;i++){
          bool isPresent(String idcart) {
            return _id.toString().contains(_selectedBarang[i].toString());
          }
          if (isPresent(_selectedBarang[i].toString()) == false) {
            cart.add({
              "id": barangdetail[category_id-1]['id'],
              "name": barangdetail[category_id-1]['name'],
              "qty": barangdetail[category_id-1]['qty'],
              "harga": double.parse(barangdetail[category_id-1]['harga']),
              "satuan": barangdetail[category_id-1]['satuan'],
              "potongan": 0.0,
              "hargatotal": double.parse(barangdetail[category_id-1]['harga']) * double.parse(barangdetail[category_id-1]['qty']),
              "hargasatuan": double.parse(barangdetail[category_id-1]['harga']),
              "sisapotongan": double.parse(barangdetail[category_id-1]['harga']) * double.parse(barangdetail[category_id-1]['qty'])
            });
          }
        }
        SaveCartToLocal();
        SaveSelectedToLocal();
      });
    } else {
      setState(() {
        _selectedBarang.remove(category_id);
        if(_selectedBarang.length < barangdetail.length){
          selectall = false;
        }
        cart.removeWhere((item) => item['id'] == category_id);
        SaveCartToLocal();
        SaveSelectedToLocal();
      });
    }
  }

  //Money Formater
  FlutterMoneyFormatter fmf = FlutterMoneyFormatter(amount: 12345678.9012345);
  //Save Local Storage
  final LocalStorage storage = new LocalStorage('DB_CATATAN_BELANJA');
  final LocalStorage storagecart = new LocalStorage('DB_CATATAN_CART');
  final LocalStorage storagebudget = new LocalStorage('DB_BUDGET');
  final LocalStorage storagehistory = new LocalStorage('DB_HISTORY');
  final LocalStorage storagetimetranc = new LocalStorage('DB_TIME');
  final LocalStorage storageselectedmore = new LocalStorage('DB_SELECTED');



  SaveCartToLocal() {
    storagecart.setItem('listcart', cart);
  }

  SaveSelectedToLocal() {
    storageselectedmore.setItem('select', _selectedBarang);
  }

  clearStorage() {
    storagecart.clear();
  }

  saveDataToLocal() {
    storage.setItem('listbarang', barangdetail);
  }

  saveData() {
    int increment;
    if (barangdetail.isNotEmpty) {
      for (int i = 0; i < barangdetail.length; i++) {
        setState(() {
          increment = barangdetail[i]['id'];
        });
      }
    } else {
      setState(() {
        increment = 0;
      });
    }
    var jsonData = {
      "id": increment + 1,
      "name": "${c_namabarang.text}",
      "qty": "${c_qty.text}",
      "satuan": "${c_satuan.text}",
      "harga": c_harga.text
    };
    setState(() {
      barangdetail.add(jsonData);
      saveDataToLocal();
    });
  }

  deleteAll(){
    for(int i=0;i<_selectedBarang.length;i++){
      setState(() {
        barangdetail.removeWhere((item)=> item['id'] == _selectedBarang[i]);
        cart.removeWhere((item)=> item['id'] == _selectedBarang[i]);
      });
    }
    setState(() {
      _selectedBarang.clear();
    });
    saveDataToLocal();
    SaveSelectedToLocal();
    SaveCartToLocal();
//    print(_selectedBarang);
//    print(cart);
//    print(barangdetail);
  }

  deleteData(_id) {
    setState(() {
      barangdetail
          .removeWhere((item) => item['id'].toString() == _id.toString());
      saveDataToLocal();
    });
//    print(barangdetail);
  }

  editData(id, nama, qty, harga, satuan) {
    final updatedata = barangdetail
        .firstWhere((item) => item['id'].toString() == id.toString());
//    print(updatedata['id'].toString());
    setState(() {
      updatedata['name'] = nama.toString();
      updatedata['qty'] = qty.toString();
      updatedata['harga'] = harga.toString();
      updatedata['satuan'] = satuan.toString();
    });
    saveDataToLocal();
  }

  addToCart(id, nama, qty, harga, potongan, satuan) {
    SaveSelectedToLocal();
    bool data = true;
    if (potongan == null) {
      setState(() {
        potongan = 0.0;
      });
    }
    double hargatotal = harga * double.parse(qty);
    double hargasatuan = harga;
    double sisapotongan = hargatotal - potongan;
    List _id = [];
    for (int i = 0; i < cart.length; i++) {
      setState(() {
        _id.add(cart[i]['id']);
      });
    }
    bool isPresent(String idcart) {
      return _id.toString().contains(id.toString());
    }
    if (isPresent(id.toString()) == true) {
//      print("masuk update");
      /*final updatedata = cart.firstWhere((item) => item['id'].toString() == id.toString());
      setState(() {
        updatedata['name'] = nama;
        updatedata['qty'] = qty;
        updatedata['harga'] = harga;
        updatedata['potongan'] = potongan;
        updatedata['hargatotal'] = hargatotal;
        updatedata['hargasatuan'] = hargasatuan;
        updatedata['sisapotongan'] = sisapotongan;
        updatedata['satuan'] = satuan.toString();
      });
      SaveCartToLocal();*/
      _showAlert(context);
    } else {
      _selectedBarang.add(id);
//      print("masuk add");
      var jsonData = {
        "id": id,
        "name": nama,
        "qty": qty,
        "harga": harga,
        "potongan": potongan,
        "hargatotal": hargatotal,
        "hargasatuan": hargasatuan,
        "sisapotongan": sisapotongan,
        "satuan": satuan.toString()
      };
      setState(() {
        cart.add(jsonData);
        SaveCartToLocal();
      });
//      print(cart);
    }
  }

  void _showAlert(context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Perhatian!"),
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            content: Container(
              height: 125,
              child: Column(
                children: <Widget>[
                  Container(
                      height: 80,
                      child: Text(
                        "Barang Sudah Pernah Ditambahkan Sebelumnya, Periksa Keranjang Belanja Anda untuk Melakukan Perubahan!",style: TextStyle(fontSize: 13),)),
                  SizedBox(
                    height: 10,
                  ),
                  Expanded(
                    flex: 9,
                    child: Container(
                        decoration: BoxDecoration(
                            color: Colors.red,
                            border: Border.all(color: Colors.black, width: 0.1),
                            borderRadius: BorderRadius.circular(10)),
                        height: 35,
                        child: FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Tutup",
                            style: TextStyle(color: Colors.white),
                          ),
                        )),
                  ),
                ],
              ),
            ),
          );
        });
  }

  void _showDisclaimer(context, id, name) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Hapus Data $name ?"),
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            content: Container(
              height: 35,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 9,
                    child: Container(
                        decoration: BoxDecoration(
                            color: Colors.red,
                            border: Border.all(color: Colors.black, width: 0.1),
                            borderRadius: BorderRadius.circular(10)),
                        height: 35,
                        child: FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Cancel",
                            style: TextStyle(color: Colors.white,fontSize: 12),
                          ),
                        )),
                  ),
                  Expanded(flex: 1, child: Container()),
                  Expanded(
                    flex: 9,
                    child: Container(
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black, width: 0.1),
                            borderRadius: BorderRadius.circular(10)),
                        height: 35,
                        child: FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                            deleteData(id);
                          },
                          child: Text("Lanjutkan",style: TextStyle(fontSize: 12)),
                        )),
                  )
                ],
              ),
            ),
          );
        });
  }

  void _showOption(context, id, name, qty, harga, satuan) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("$name"),
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            content: Container(
              height: 125,
              child: Column(
                children: <Widget>[
                  Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black, width: 0.1),
                          borderRadius: BorderRadius.circular(10)),
                      height: 35,
                      child: FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                          _showDisclaimer(context, id, name);
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Delete"),
                            Icon(Icons.delete),
                          ],
                        ),
                      )),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black, width: 0.1),
                          borderRadius: BorderRadius.circular(10)),
                      height: 35,
                      child: FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                          _showDialog(
                              context, 'edit', id, name, qty, harga, satuan);
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Edit"),
                            Icon(Icons.mode_edit),
                          ],
                        ),
                      )),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black, width: 0.1),
                          borderRadius: BorderRadius.circular(10)),
                      height: 35,
                      child: FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                          _showDialog(context, "addtocart", id, name, qty,
                              harga, satuan);
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Add To cart"),
                            Icon(Icons.add_shopping_cart),
                          ],
                        ),
                      )),
                ],
              ),
            ),
          );
        });
  }

  void _showDialog(context, param, id, name, qty, harga, satuan) {
    if (param == 'add') {
      setState(() {
        add = true;
      });
    } else if (param == 'addtocart') {
      setState(() {
        add = false;
        c_namabarang.text = name;
        c_harga.text = harga;
        c_qty.text = qty;
      });
    } else if (param == 'edit') {
      setState(() {
        c_namabarang.text = name;
        c_harga.text = harga;
        c_qty.text = qty;
      });
    }
    _dropDownSatuan = buildAndGetDropDownMenuSatuan();
    c_satuan.text = _dropDownSatuan[0].value;
    if (satuan.toString().length != 0) {
      setState(() {
        c_satuan.text = satuan;
      });
    }
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return alert dialog object
        return AlertDialog(
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          title: param == 'addtocart'
              ? Text("Add To Cart")
              : param == 'add'
              ? Text('Tambah List Barang')
              : Text('Edit List Barang'),
          content: SingleChildScrollView(
            child: Container(
              height: param == 'addtocart' ? 210 : 165,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: 35,
                    child: TextFormField(
                      controller: c_namabarang,
                      decoration: InputDecoration(
                        hintText: "Masukan Nama Barang",
                        fillColor: Colors.white,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10),
                          borderSide: new BorderSide(),
                        ),
                        contentPadding: EdgeInsets.only(left: 10, right: 10),
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 9,
                          child: Container(
                            height: 35,
                            child: TextFormField(
                              controller: c_qty,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                hintText: "Masukan Quantity",
                                fillColor: Colors.white,
                                border: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(10),
                                  borderSide: new BorderSide(),
                                ),
                                contentPadding:
                                EdgeInsets.only(left: 10, right: 10),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: SizedBox(width: 5),
                        ),
                        Expanded(
                          flex: 5,
                          child: Container(
                            height: 35,
                            width: MediaQuery.of(context).size.longestSide,
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                isExpanded: true,
                                value: c_satuan.text,
                                items: _dropDownSatuan,
                                onChanged: (select) {
                                  if (select.toString() != null ||
                                      select.toString() != '') {
                                    setState(() {
                                      Navigator.of(context).pop();
                                      _showDialog(context, "$param", "$id",
                                          "$name", "$qty", "$harga", "$satuan");
                                      valueSatuan = select.toString();
                                      c_satuan.text = select.toString();
                                    });
                                  }
                                },
                                hint: Text("Satuan"),
                              ),
                            ),
                            decoration: new BoxDecoration(
                              border: new Border.all(color: Colors.black38),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            padding: const EdgeInsets.only(left: 10),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    height: 35,
                    child: TextFormField(
                      controller: c_harga,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        hintText: "Masukan Harga Barang",
                        fillColor: Colors.white,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10),
                          borderSide: new BorderSide(),
                        ),
                        contentPadding: EdgeInsets.only(left: 10, right: 10),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  param == 'addtocart'
                      ? Container(
                    height: 35,
                    child: TextFormField(
                      controller: c_potongan,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        hintText: "Masukan Potongan",
                        fillColor: Colors.white,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10),
                          borderSide: new BorderSide(),
                        ),
                        contentPadding:
                        EdgeInsets.only(left: 10, right: 10),
                      ),
                    ),
                  )
                      : Container(),
                  param == 'addtocart' ? SizedBox(height: 10) : Container(),
                  Container(
                    alignment: Alignment.centerRight,
                    height: 30,
                    child: param == 'add'
                        ? FlatButton(
                        onPressed: () {
                          if (c_satuan.text.length == 0 ||
                              c_namabarang.text.length == 0 ||
                              c_qty.text.length == 0 ||
                              c_harga.text.length == 0) {
                            Fluttertoast.showToast(
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                msg: "Semua Field Harus Diisi !",
                                backgroundColor: Colors.red,
                                textColor: Colors.white);
                          } else {
                            Navigator.pop(context);
                            saveData();
                            c_harga.clear();
                            c_qty.clear();
                            c_namabarang.clear();
                            c_satuan.clear();
                          }
                        },
                        color: Colors.blue,
                        splashColor: Colors.amberAccent,
                        padding: EdgeInsets.zero,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: Container(
                          child: Text(
                            "Submit",
                            style:
                            TextStyle(fontSize: 14, color: Colors.white),
                          ),
                        ))
                        : param == 'addtocart'
                        ? FlatButton(
                        onPressed: () {
                          if (c_satuan.text.length == 0 ||
                              c_namabarang.text.length == 0 ||
                              c_qty.text.length == 0 ||
                              c_harga.text.length == 0) {
                            Fluttertoast.showToast(
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                msg: "Semua Field Harus Diisi !",
                                backgroundColor: Colors.red,
                                textColor: Colors.white);
                          } else {
                            Navigator.pop(context);
                            if (c_potongan.text.length == 0) {
                              setState(() {
                                c_potongan.text = "0";
                              });
                            }
                            double potongan =
                            double.parse(c_potongan.text);
                            setState(() {
                              id = id;
                              name = c_namabarang.text;
                              qty = c_qty.text;
                              harga = double.parse(c_harga.text);
                            });
                            addToCart(
                                id, name, qty, harga, potongan, satuan);
                            c_harga.clear();
                            c_qty.clear();
                            c_namabarang.clear();
                            c_satuan.clear();
                            c_potongan.clear();
                          }
                        },
                        color: Colors.blue,
                        splashColor: Colors.amberAccent,
                        padding: EdgeInsets.zero,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 5, vertical: 3),
                          child: Text(
                            "Add To Cart",
                            style: TextStyle(
                                fontSize: 14, color: Colors.white),
                          ),
                        ))
                        : FlatButton(
                        onPressed: () {
                          if (c_satuan.text.length == 0 ||
                              c_namabarang.text.length == 0 ||
                              c_qty.text.length == 0 ||
                              c_harga.text.length == 0) {
                            Fluttertoast.showToast(
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                msg: "Semua Field Harus Diisi !",
                                backgroundColor: Colors.red,
                                textColor: Colors.white);
                          } else {
                            Navigator.pop(context);
//                            print(c_satuan.text);
                            editData(id, c_namabarang.text, c_qty.text,
                                c_harga.text, c_satuan.text);
                            c_harga.clear();
                            c_qty.clear();
                            c_namabarang.clear();
                            c_satuan.clear();
                          }
                        },
                        color: Colors.blue,
                        splashColor: Colors.amberAccent,
                        padding: EdgeInsets.zero,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: Container(
                          child: Text(
                            "Update",
                            style: TextStyle(
                                fontSize: 14, color: Colors.white),
                          ),
                        )),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  var jsonsearch;
  bool _search = false;
  List search = [];
  listtemp() {
//    print('masuk search tmp');
    setState(() {
      search = barangdetail;
    });
//    print(search);
  }

  void filterSearchResults(String query) {
    List tmpsearch = [];
    setState(() {
      tmpsearch = search;
    });
    jsonsearch = null;
    if (query.isNotEmpty) {
      List templagi = [];
      tmpsearch.forEach((item) {
//        print(item);
        if (item['name']
            .toString()
            .toUpperCase()
            .contains(query.toUpperCase())) {
          jsonsearch = {
            "id": item['id'],
            "name": item['name'],
            "qty": item['qty'],
            "satuan": item['satuan'],
            "harga": item['harga']
          };
          templagi.add(jsonsearch);
        }
        /*else{
          print("masuk else foreach");
          setState(() {
            _loading = true;
            barangdetail.clear();
//            _loading = true;
          });
        }*/
        setState(() {
          _loading = false;
          barangdetail.clear();
          barangdetail.addAll(templagi);
        });
        return;
      });
//      return;
    } else {
      setState(() {
        barangdetail.clear();
        barangdetail.addAll(search);
        _loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    List _datatranc = storagehistory.getItem('transaksi');
    if (_datatranc != null) {
      setState(() {
        datatranc = _datatranc;
      });
    }else{
      setState(() {
        datatranc = [];
      });
    }
    var budget = storagebudget.getItem('budget');
    if (budget.toString().length != 0) {
      setState(() {
        budgetku = budget;
      });
    }
    if (barangdetail.isEmpty) {
      List data = storage.getItem('listbarang');
      if (data != null) {
        temp = data;
        if (temp.length != null) {
          setState(() {
            barangdetail = temp;
            tempsearch = temp;
            _loading = false;
          });
        } else {
          setState(() {
            barangdetail = [];
            _loading = false;
          });
        }
      } else {
        setState(() {
          barangdetail = [];
          _loading = false;
        });
      }
    }
    if(cart.isEmpty){
      List listdata = storagecart.getItem('listcart');
      if (listdata != null) {
        tempcart = listdata;
        if (tempcart.length != null) {
          setState(() {
            cart = tempcart;
            _loading = false;
            if(cart.length == barangdetail.length){
              selectall = true;
            }
          });
        } else {
          setState(() {
            cart = [];
            _loading = false;
            selectall = false;
          });
        }
      } else {
        setState(() {
          cart = [];
          selectall = false;
        });
      }
    }else{
      setState(() {
        if(cart.length == barangdetail.length){
          selectall = true;
        }else{
          selectall = false;
        }
      });
    }
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          top: false,
          bottom: false,
          minimum: EdgeInsets.only(top: 25),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                Container(
                  height: 50,
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
//                        color: Colors.amber,
                            ),
                            child: FlatButton(
                                padding: const EdgeInsets.all(0),
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Icon(Icons.add, size: 25),
                                ),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25)),
                                splashColor: Colors.amberAccent,
                                onPressed: () {
                                  _showDialog(
                                      context, "add", "", "", "", "", "");
                                }),
                          ),
                        ),
                        Expanded(
                          flex: 11,
                          child: Container(
                            height: 35,
                            child: visible
                                ? TextFormField(
                              autofocus: true,
                              controller: c_editingController,
                              onChanged: (value) {
                                filterSearchResults(value);
                              },
                              decoration: InputDecoration(
                                  hintText: "Search",
                                  prefixIcon: Icon(Icons.search),
                                  contentPadding: EdgeInsets.all(0),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(25.0)))),
                            )
                                : Container(
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.center,
                                children: <Widget>[
                                  Expanded(
                                      flex: 7,
                                      child: Align(
                                          alignment: Alignment.center,
                                          child: Text(
                                            "Catatan Belanjaku",
                                            style:
                                            TextStyle(fontSize: 20),overflow: TextOverflow.ellipsis,
                                          ))),
                                  Expanded(
                                    flex: 1,
                                    child: Container(
                                      width: 50,
                                      height: 50,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                        BorderRadius.circular(25),
//                        color: Colors.amber,
                                      ),
                                      child: FlatButton(
                                          padding:
                                          const EdgeInsets.all(0),
                                          child: Container(
                                            alignment: Alignment.center,
                                            child: Icon(
                                              Icons.history,
                                              size: 25,
                                            ),
                                          ),
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                              BorderRadius.circular(
                                                  25)),
                                          splashColor:
                                          Colors.amberAccent,
                                          onPressed: () {
                                            setState(() {
                                              /*datatranc.length == 0 ? Fluttertoast.showToast(
                                                    msg: "History Tidak Tersedia",
                                                    backgroundColor: Colors.orange,
                                                    textColor: Colors.white,
                                                    gravity: ToastGravity.CENTER,
                                                    toastLength: Toast.LENGTH_SHORT
                                                ) : */
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          History(
                                                              datatranc:
                                                              datatranc)));
                                            });
                                          }),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
                            ),
                            child: FlatButton(
                                padding: const EdgeInsets.all(0),
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Icon(
                                    Icons.attach_money,
                                    size: 25,
                                  ),
                                ),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25)),
                                splashColor: Colors.amberAccent,
                                onPressed: () {
                                  //to webview
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => UangKas()));
                                }),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
//                        color: Colors.amber,
                            ),
                            child: FlatButton(
                                padding: const EdgeInsets.all(0),
                                child: Container(
                                  alignment: Alignment.center,
                                  child: visible
                                      ? Icon(Icons.close, size: 25)
                                      : Icon(Icons.search, size: 25),
                                ),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25)),
                                splashColor: Colors.amberAccent,
                                onPressed: () {
                                  setState(() {
                                    /* listtemp();
                                      visible = !visible;
                                      _loading = false;
                                      _search = false;*/
                                    Fluttertoast.showToast(
                                        msg: "Coming Soon",
                                        backgroundColor: Colors.orange,
                                        textColor: Colors.white,
                                        gravity: ToastGravity.CENTER,
                                        toastLength: Toast.LENGTH_SHORT);
                                  });
                                }),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  height: 20,
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: onlongpress ? 3 : 2,
                        child:  onlongpress ? Container(
                            height: 10,
                            width: 0,
                            child: Stack(
                              alignment: Alignment.centerLeft,
                              children: <Widget>[
                                Positioned(
                                  child: Checkbox(
                                    activeColor: Colors.orange,
                                    value: selectall,
                                    onChanged: (bool selected){
                                      setState(() {
                                        selectall = !selectall;
                                        _selectAll(selectall);
                                      });
                                    },
                                  ),
                                  right: 15,
                                )
                              ],
                              overflow: Overflow.visible,
                            )
                        ) : Text("No", textAlign: TextAlign.left),
                      ),
                      Expanded(
                        flex: 5,
                        child: Text(" Nama Barang"),
                      ),
                      Expanded(
                        flex: 3,
                        child: Text("Quantity", textAlign: TextAlign.center),
                      ),
                      Expanded(
                        flex: 5,
                        child: Text("Harga", textAlign: TextAlign.center),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                _loading
                    ? Container(
                    height: MediaQuery.of(context).size.height / 1.2,
                    child: Center(
                      child: CircularProgressIndicator(),
                    ))
                    : barangdetail.length == null || barangdetail.length == 0
                    ? Container(
                    height: MediaQuery.of(context).size.height / 1.2,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Icon(
                            Icons.do_not_disturb_alt,
                            size: 100,
                            color: Colors.grey[800],
                          ),
                          Text("Data Kosong",
                              style: TextStyle(
                                  fontSize: 30,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[800]),
                              textAlign: TextAlign.center),
                          Text(
                              "Tambahkan Inventory Dengan Tap Icon +",
                              style: TextStyle(
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[800]),
                              textAlign: TextAlign.center)
                        ],
                      ),
                    ))
                    : Stack(
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height - 125,
                      child:
                      /*_search ? ListView.builder(
                        itemCount: search.length,
                        shrinkWrap: true,
                        physics: AlwaysScrollableScrollPhysics(),
                        itemBuilder: (BuildContext context, int index){
                          var id = search[index]['id'];
                          return Padding(
                            padding: const EdgeInsets.symmetric(vertical: 5),
                            child: Container(
                              alignment: Alignment.center,
                              child: GestureDetector(
                                onTap: (){
                                  _showOption(context,id,search[index]['name'],search[index]['qty'],search[index]['harga']);
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Expanded(
                                      flex: 1,
                                      child: Text("${index+1}",textAlign: TextAlign.center),
                                    ),
                                    Expanded(
                                      flex: 5,
                                      child: Text("${search[index]['name']}"),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: Text("${search[index]['qty']} ${search[index]['satuan']}",textAlign: TextAlign.center),
                                    ),
                                    Expanded(
                                      flex: 5,
                                      child: Text("${
                                          fmf.copyWith(symbol : 'Rp',symbolAndNumberSeparator:".",amount:double.parse(search[index]['harga'])).output.symbolOnLeft
                                      }",textAlign: TextAlign.center),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        }) :*/
                      ListView.builder(
                          itemCount: barangdetail.length,
                          shrinkWrap: true,
                          physics: AlwaysScrollableScrollPhysics(),
                          itemBuilder:
                              (BuildContext context, int index) {
                            var id = barangdetail[index]['id'];
                            return Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 3, horizontal: 3),
                              child: Container(
                                padding: EdgeInsets.symmetric(horizontal: 0,vertical: 10),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.grey[200],
                                ),
                                alignment: Alignment.center,
                                child: GestureDetector(
                                  onTap: () {
                                    moreselection ? Container() :  _showOption(
                                        context,
                                        id,
                                        barangdetail[index]['name'],
                                        barangdetail[index]['qty'],
                                        barangdetail[index]
                                        ['harga'],
                                        barangdetail[index]
                                        ['satuan']);
                                  },
                                  onLongPress: (){
                                    if(_selectedBarang.length != 0){
                                      if(cart.length == 0){
                                        setState(() {
                                          _selectedBarang.clear();
                                        });
                                      }
                                    }
                                    setState(() {
                                      moreselection = !moreselection;
                                      onlongpress = !onlongpress;
                                      _onBarangSelected(onlongpress, barangdetail[index]['id']);
                                    });
                                  },
                                  child: onlongpress ? CheckboxListTile(
                                    activeColor: Colors.orange,
                                    value: _selectedBarang.contains(barangdetail[index]['id']),
                                    onChanged: (bool selected){
                                      _onBarangSelected(selected, barangdetail[index]['id']);
                                    },
                                    controlAffinity: ListTileControlAffinity.leading,
                                    title: Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment
                                          .spaceBetween,
                                      children: <Widget>[
                                        Expanded(
                                          flex: 5,
                                          child: Text(
                                              "${barangdetail[index]['name']}"),
                                        ),
                                        Expanded(
                                          flex: 3,
                                          child: Text(
                                              "${barangdetail[index]['qty']} ${barangdetail[index]['satuan']}",
                                              textAlign:
                                              TextAlign.center),
                                        ),
                                        Expanded(
                                          flex: 5,
                                          child: Container(
                                            padding: EdgeInsets.symmetric(horizontal: 5),
                                            child: Text(
                                                "${fmf.copyWith(symbol: 'Rp', symbolAndNumberSeparator: ".", amount: double.parse(barangdetail[index]['harga'])).output.symbolOnLeft}",
                                                textAlign:
                                                TextAlign.right),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ) : Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment
                                        .spaceBetween,
                                    children: <Widget>[
                                      Expanded(
                                        flex: 2,
                                        child: Container(
                                          padding: EdgeInsets.symmetric(horizontal: 5),
                                          child: Text("${index + 1}.",
                                              textAlign:
                                              TextAlign.left),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 5,
                                        child: Text(
                                            "${barangdetail[index]['name']}"),
                                      ),
                                      Expanded(
                                        flex: 3,
                                        child: Text(
                                            "${barangdetail[index]['qty']} ${barangdetail[index]['satuan']}",
                                            textAlign:
                                            TextAlign.center),
                                      ),
                                      Expanded(
                                        flex: 5,
                                        child: Container(
                                          padding: EdgeInsets.symmetric(horizontal: 5),
                                          child: Text(
                                              "${fmf.copyWith(symbol: 'Rp', symbolAndNumberSeparator: ".", amount: double.parse(barangdetail[index]['harga'])).output.symbolOnLeft}",
                                              textAlign:
                                              TextAlign.right),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }),
                    ),
                    Positioned(
                      bottom: 0,
                      left: 0,
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: onlongpress == false? Container() :  _selectedBarang.length == 0 ?
                        Container() : Stack(
                          overflow: Overflow.visible,
                          children: <Widget>[
                            Container(
                              width: 50,
                              height: 50,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(25),
                                color: Colors.red,
                              ),
                              child: FlatButton(
                                  padding: const EdgeInsets.all(0),
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Icon(
                                      Icons.delete,
                                      size: 25,
                                      color: Colors.white,
                                    ),
                                  ),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(25)),
                                  splashColor: Colors.grey,
                                  onPressed: () {
                                    deleteAll();
                                    if(barangdetail.length == 0){
                                      setState(() {
                                        onlongpress = false;
                                      });
                                    }
                                  }),
                            ),
                            Positioned(
                              top: 0,
                              right: _selectedBarang.length > 99
                                  ? -12
                                  : _selectedBarang.length > 9 && _selectedBarang.length < 100 ? -6 : -3,
                              child: _selectedBarang.length == 0
                                  ? Container()
                                  : Container(
                                padding: EdgeInsets.symmetric(horizontal: _selectedBarang.length == 1 ? 7.5 : 6, vertical: 2),
                                decoration: BoxDecoration(
                                    color: Colors.orange,
                                    borderRadius: BorderRadius.circular(30)),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("${_selectedBarang.length}",
                                      style: TextStyle(fontSize: 12, color: Colors.white),textAlign: TextAlign.center),
                                ),
                              ),
                            ),
                          ],
                        )
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          Container(
            width: 50,
            height: 50,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25),
              color: cart.length == null || cart.length == 0
                  ? Colors.grey[700]
                  : Colors.blue,
            ),
            child: FlatButton(
                padding: const EdgeInsets.all(0),
                child: Container(
                  alignment: Alignment.center,
                  child: Icon(
                    Icons.add_shopping_cart,
                    size: 25,
                    color: Colors.white,
                  ),
                ),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25)),
                splashColor: cart.length == null || cart.length == 0
                    ? Colors.grey
                    : Colors.amberAccent,
                onPressed: () {
                  setState(() {
                    moreselection = false;
                    onlongpress = false;
                  });
                  cart.length == null || cart.length == 0
                      ? Fluttertoast.showToast(
                      msg: "Keranjang Belanja Kosong, Silahkan Tambah Barang Ke Keranjang Belanja Anda",
                      backgroundColor: Colors.orange,
                      gravity: ToastGravity.BOTTOM,
                      textColor: Colors.white)
                      : Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Cart(
                              List: cart,
                              Inventory: barangdetail,
                              budget: budgetku,
                              history: datatranc,
                              selected : _selectedBarang)));
                }),
          ),
          Positioned(
            top: 0,
            left: cart.length > 99
                ? -12
                : cart.length > 9 && cart.length < 100 ? -6 : -3,
            child: cart.length == 0
                ? Container()
                : Container(
              padding: EdgeInsets.symmetric(horizontal: cart.length == 1 ? 7.5 : 6, vertical: 2),
              decoration: BoxDecoration(
                  color: Colors.orange,
                  borderRadius: BorderRadius.circular(30)),
              child: Align(
                alignment: Alignment.center,
                child: Text("${cart.length}",
                    style: TextStyle(fontSize: 12, color: Colors.white),textAlign: TextAlign.center),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
